function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.




}    



function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.

    let word = "isogram";

    for (let i = 0; i < word.length; i++){

        if(
            word[i].toLowerCase() == "i" &&
            word[i].toLowerCase() == "s" &&
            word[i].toLowerCase() == "o" &&
            word[i].toLowerCase() == "g" &&
            word[i].toLowerCase() == "r" &&
            word[i].toLowerCase() == "a" &&
            word[i].toLowerCase() == "m" 
            ){
            return true
           
        }else{
            return false;
            }
    }

}

function purchase(age) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    const price = 109.4356;
    const discountedPrice = price * 0.8;
    const roundedPrice = discountedPrice.toFixed(2);

    if(age < 13){
        return undefined;
    }else if (age ==13 || age <= 21 || age >= 65){
        return (`${roundedPrice}`);
    }else if (age ==22 || age <= 64)
        return (`${price.toFixed(2)}`);
    
}

pricePerAgeBracket = purchase(60);
console.log(pricePerAgeBracket);



function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    return items;
}

    let items = [

        { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
        {id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
        {id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
        {id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
        {id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }  

        ];

    let hotCategories = items.includes("stocks: 0");
    console.log(hotCategories);



function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:

    return (candidateA, candidateB);   

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

let candidateA = ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
let candidateB = ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
console.log([(candidateA[0] && candidateB[5]), (candidateA[1] && candidateB[4])])




module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};



